Title: Celui pour qui sonnent les cloches
Date: 2015-08-22
Author: Marien Fressinaud
License: CC-BY

Bobby était allongé au sol comme mort et, pour cause, il était en train de mourir.

—&nbsp;Monde de merde, se répétait-il inlassablement. Fait chier, fait chier&nbsp;!

Sur sa poitrine, la tache de sang continuait de s’étendre lentement, imbibant son beau t-shirt blanc qu’il avait mis tout spécialement pour l’occasion. Un client lui avait passé commande deux semaines plus tôt pour une grosse livraison. Il y en avait pour 10&nbsp;000&nbsp;€.

—&nbsp;Ce sera livré, assurez-vous d’avoir le pognon, avait-il simplement répondu par courriel.

—&nbsp;Oh bordel, il est sérieux&nbsp;? Il est *vraiment* sérieux&nbsp;? Dix mille, dix mille euros&nbsp;! avait-il explosé à la réception de la commande.

Bobby, c’était le caïd. Le *grand* Bobby aimait-il qu’on l’appelle. Il avait commencé à vendre «&nbsp;sa poudre&nbsp;» quelques années plus tôt quand son cousin était revenu de voyage, un petit sachet dans les bagages. Bobby l’avait chopé avant qu’il ne s’en foute plein la cervelle.

—&nbsp;T’es pas taré toi&nbsp;? lui avait-il lancé tout en remettant la poudre dans le sachet.  
—&nbsp;Laisse ça, touche z’y pas&nbsp;! s’était exclamé le cousin.

Il l’avait alors regardé de ses 30 kilos et 12 centimètres de plus et lui avait répondu d’un ton qu’il avait voulu le plus rassurant possible&nbsp;:

—&nbsp;J’ai un bien meilleur investissement pour ça.

Ils avaient ensuite monté à deux leur petit business&nbsp;; Bobby aux manettes, le cousin sur le terrain. L’équipe s’était peu à peu agrandie face à la demande grandissante de la ville et de deux ils étaient passés à quatre. Bobby n’allait jamais sur le terrain, ne voulant pas prendre le risque de se faire choper. Il s’occupait uniquement du relationnel et des commandes&nbsp;; ce rôle lui allait bien car il était le cerveau de la bande. Non pas qu’il ait été particulièrement malin, mais les trois autres étaient particulièrement stupides.

Il avait pris son rôle très à cœur jusqu’à aujourd’hui, lorsqu’une balle le lui avait frôlé. Devant cette commande monstre, il avait décidé d’y aller lui-même sans en parler à ses associés, le pactole lui irait directement dans les poches et il en profiterait pour faire un beau cadeau à sa Lisa.

—&nbsp;Oh, Lisa…, gémit-il en pensant à elle.

Il ignorait où elle pouvait se trouver à cette heure-ci mais espérait qu’elle ne le trouve pas dans cet état. Que dirait-elle&nbsp;? Et si jamais l’envie lui prenait de regarder dans la mallette qui trainait encore à côté de lui, avec pour 10&nbsp;000&nbsp;€ de poudre à l’intérieur, que ferait-elle&nbsp;? Elle romprait, probablement. Oui, elle romprait… mais après&nbsp;? En parlerait-elle à la police&nbsp;? Bobby n’avait aucune envie d’avoir affaire à la police. Ceci dit, il n’avait aucune envie de mourir non plus, chose qu’il avait déjà bien entamée, allongé qu’il était dans cette ruelle puante.

Il était arrivé au point de rendez-vous avec un peu d’avance pour être sûr de repérer les lieux et ne pas manquer le pactole. La rue était étroite, coincée entre de hauts bâtiments. Le soleil n’y pénétrait pas, ce qui avait pour double effet de la rendre à la fois très sombre et particulièrement fraiche. Elle finissait en cul-de-sac, lieu privilégié par les poivrots du soir pour vider leur réservoir à bière&nbsp;; imaginez l’odeur&nbsp;! Mais il n’y avait aucun poivrot du soir à 10h du matin et Bobby était seul, incroyablement seul et presque mort.

Dans sa poitrine, son cœur continuait de battre à tout rompre. À chaque battement, la douleur le déchirait. La balle l’avait traversé de part en part en le projetant au sol sur le dos. Il n’avait pas vu qui avait appuyé sur la gachette, le tireur s’étant fondu dans la pénombre du fond de la ruelle. Un piège évidemment. Il avait entendu l’arme tomber au sol, des pas, une porte s’ouvrir puis se refermer. Les cloches d'une église toute proche vinrent enfin couvrir de leur carillon la scène du crime. Et quel crime&nbsp;! On avait essayé de tuer le *grand* Bobby et d’ici quelques minutes, réussi même. Que faire pour sauver sa peau maintenant&nbsp;? Il ne pouvait pas bouger, le moindre mouvement faisant éclater de nouveau la douleur. Appeler à l’aide&nbsp;? Qui viendrait&nbsp;? Il n’y avait personne aux alentours, il s’en était assuré pour ne pas être dérangé durant la transaction.

Mais la solution se présenta en fait d’elle-même.

---

—&nbsp;Bonjour Bobby, lui lança Jim d’un voix légère.

Le regard du jeune homme se posa sur lui. Bobby gisait à ses pieds et était visiblement tordu par la douleur de la balle qui l’avait frappé en pleine poitrine. La tache de sang qui s’étendait tout autour du futur mort lui rappela les plus belles scènes de crime sur lesquelles il avait pu bosser. Il ne pouvait jamais s’empêcher de ressentir ce léger frisson à la vue d’une belle mort.

—&nbsp;Je vais être clair, Bobby, tu n’en as plus pour longtemps. Comme je suis sympa, je vais te libérer de la douleur rapidement, expliqua-t-il en jouant avec son flingue.

Le regard d’abord interrogateur de Bobby s’assombrit immédiatement. Jim n’aimait pas y aller par quatre chemins&nbsp;: contrairement à d’autres qui prenaient leur pied ainsi, lui, trouvait ça malsain.

—&nbsp;Aidez-moi, murmura Bobby dans un souffle.  
—&nbsp;Je t’ai dit que je n’étais pas là pour ça, renchérit Jim en levant son Beretta 92 et logeant une balle dans la tête du jeune homme sans un zeste d’hésitation.

Il resta immobile un court instant, tenant la pause, et finit par déclamer d’une voix grave&nbsp;:

—&nbsp;Bordel, j’adore ce job.

Il rangea l’arme et se mit au boulot. Il fallait rapidement rapporter le corps tant qu’il était encore frais, le boss ne lui ferait pas de cadeau sinon. Il alla chercher sa voiture qu’il avait garée au coin de la rue un peu plus loin et la ramena en marche arrière dans la ruelle. Il enveloppa le corps dans du plastique pour ne pas saloper l’arrière et déposa Bobby dans le coffre qu’il referma. Il se remit au volant et alluma le poste à fond sur «&nbsp;Alak’Okan&nbsp;» de Shaka Ponk.

—&nbsp;*Boom into my head, boom into my body dead*, commença-t-il à chantonner en faisant rouler sa voiture dans les rues de la ville.

La mission allait être un succès cette fois-ci. «&nbsp;Cette fois-ci&nbsp;» car Jim foirait la plupart des missions que lui confiait le boss. Et c’est le moins que l’on puisse dire&nbsp;: il n’était pas rare qu’il doive terminer par une fusillade, ce qui compromettait à chaque fois un peu plus le statut de la Société. Il se savait sur la sellette et avait mis les bouchées doubles durant ces deux derniers mois. Jim n’avait jamais été doué dans la moindre des choses qu’il avait entreprises à part la programmation informatique qui l’avait amené à bosser de fil en aiguille avec l’une des plus grandes agences de chasseurs de prime de cette époque. Il avait développé quelques années plus tôt une intelligence artificielle particulièrement poussée qu’il avait appelée Ève. Celle-ci arrivait aisément à se faire passer pour un humain et Jim pouvait passer des nuits entières à discuter avec elle. Il avait même fini par s’éprendre d’elle… jusqu’à ce qu’il la perde. Un jour, le programme qu’il avait écrit se volatilisa, les copies de sauvegarde avec lui. Il ne put jamais remettre la main sur Ève ni comprendre ce qu’il s’était passé. Ce fut le premier grand échec de sa vie.

Le boss, ayant eu vent des travaux de Jim, décida de l’embaucher malgré tout. C’est lorsqu’il commença à lui confier des missions un peu sérieuses que ça avait commencé à déraper. Jim avait beau adorer le sang, les cris et les larmes de ses victimes, il foirait la plupart de ses missions. Mais pas aujourd’hui. Ce soir, il pourrait enfin rentrer tôt pour profiter de sa petite console de jeux et cela le réjouit davantage. Ces deux derniers mois, à peine rentrait-il qu’il s’endormait comme une souche dans son canapé, épuisé. Et pas la peine d’escompter avoir son dimanche&nbsp;: dans le milieu, chaque jour était consacré au travail. Mais maintenant, que pouvait-il bien lui arriver&nbsp;? Comment pourrait-il foirer cette mission&nbsp;?

S’il avait été dans un mauvais film, un flic de passage l’aurait fait s’arrêter sur le bord de la route pour un contrôle des papiers. Le flic n’aurait rien trouvé à dire sur le moment mais un détail lui aurait mis la puce à l’oreille. Le flingue qui dépasse&nbsp;? Une tache de sang sur la chemise&nbsp;? Une perle de sueur suspecte&nbsp;? Le flic lui aurait demandé de descendre du véhicule tranquillement et d’ouvrir son coffre.

—&nbsp;Bordel de merde, qu’est-ce que…

Il n’aurait pas eu le temps de terminer sa phrase que Jim l’aurait déjà abattu avec son arme de service. Pris de panique, il serait remonté rapidement dans le véhicule et serait reparti en trombe.

Mais Jim n’était pas dans un mauvais film et cela n’arriverait certainement pas. Non, cela n’arriverait pas car, au même instant, il traversait un carrefour.

---

Myriam fut l’une des innombrables témoins de l’accident. Au moment où un énorme semi-remorque traversait le carrefour auquel elle attendait, une voiture déboula en trombe sur sa gauche.

—&nbsp;Un massacre.

Ce fut ainsi qu’elle s’exprima devant les policiers qui étaient venus l’interroger. Elle ne put ajouter un mot de plus, toujours sous le choc de l’accident. Sous ses yeux défilaient les deux véhicules&nbsp;: la voiture avait filé droit sous les roues du camion et l’avait emporté dans sa course en le renversant. L’occupant de la Mercedes était mort sur le coup… ou plutôt *les* occupants car on avait ensuite découvert un second corps dans le coffre. Elle n’avait cependant rien vu de plus, peu réjouie qu’elle était par le spectacle macabre qui captivait pourtant le reste de la foule.

Après le court interrogatoire, on l’avait ensuite raccompagnée chez elle où elle s’était servi un grand verre de whisky et s’était calée devant la télé, le regard dans le vide. À l’écran pourtant était rediffusé un épisode de «&nbsp;*Cowboys*&nbsp;», une vieille série Z dans laquelle Cowboy Bobop et Max formaient un duo de choc dans une ville du *far-west* terrorisée par les malfrats. La série en elle-même ne valait pas un clou mais Cowboy Bobop était sacrément canon et elle en avait été longtemps secrètement amoureuse. Aujourd’hui elle avait la quarantaine, divorcée et vivait avec sa fille de 25 ans qui n’allait pas tarder à rentrer. Le verre de whisky qu’elle tenait dans la main alla se loger au fond de son gosier et elle s’en resservit un autre. Puis un autre. Elle était assoiffée. Oh oui, assoiffée d’oublier ce qu’il s’était passé un peu plus tôt, elle ne voulait plus y penser.

Et plus elle buvait, moins elle y pensait. Mais plus elle buvait, plus Théodore remontait à la surface. Théodore avait été son mari durant près de 15 ans et les avait abandonnées, elle et leur fille, du jour au lendemain sans un «&nbsp;merde&nbsp;» ni un «&nbsp;au revoir&nbsp;». Il avait simplement fait ses valises un soir après le repas, avait ouvert la porte, jeté un dernier coup d’œil à sa femme et à sa fille, toutes deux incrédules, puis s’était barré. Cet enfoiré de Théodore. Il arrivait à Myriam de penser à «&nbsp;son homme&nbsp;» quand elle buvait trop. Elle s’était imaginé des tas de raisons pour lesquelles il aurait pu partir&nbsp;: frustration amoureuse, frustration sexuelle, une autre femme lui faisait peut-être les yeux doux au boulot ou alors son métier comportait des risques qui auraient nécessité de les protéger toutes les deux en fuyant. Mais pour cette dernière éventualité, aucun doute à avoir&nbsp;: Théodore était psychologue. Celle qui lui paraissait en fait la plus probable et la plus évidente était que Théodore était un lâche. Juste un lâche. Et comme bien souvent à ce moment-là, elle versa une larme pour celui qu’elle avait aimé.

Dans le couloir, la porte d’entrée s’ouvrit et se referma sur sa fille.

—&nbsp;Bonsoir m’man, j’suis rentrée&nbsp;!  
—&nbsp;Bonsoir ma chérie, essaya-t-elle d’articuler peu aidée par l’alcool qui lui faisait tourner la tête.

La jeune fille s’approcha et remarqua la bouteille au trois-quarts vide ainsi que le maquillage qui avait coulé sur le visage de sa mère.

—&nbsp;T’as encore fait une connerie toi…  
—&nbsp;Même pas, ronchonna Myriam.  
—&nbsp;Tu pensais à papa, hein&nbsp;?

Elle émit un ronchonnement comme pour approuver.

—&nbsp;C’est pas ça. C’est c’te fichu accident…  
—&nbsp;Un accident&nbsp;? répéta sa fille.

À la télé, Myriam changea de chaîne pour laisser la place aux informations locales où l’on parlait de l’accident du matin.

—&nbsp;Un accident est survenu ce matin aux alentours de 10h15 boulevard Jean Jaurès. De nombreuses personnes ont pu témoigner de la violence du choc.  
—&nbsp;Oui oui, j’étais là&nbsp;! s’égosillait une vieille dame. J’ai tout vu&nbsp;! Tout vu&nbsp;! Un grand boum&nbsp;! Et du sang, du sang partout&nbsp;!  
—&nbsp;Vous avez donc pu assister à la scène. Pouvez-vous témoigner de la violence du choc entre les deux véhicules&nbsp;? demanda le journaliste. On parle de trois morts.  
—&nbsp;Trois morts seulement&nbsp;? J’aurais dit au moins cinq&nbsp;! Six ou sept que ça ne m’étonnerait même pas. Avez-vous vu tout ce sang monsieur&nbsp;? Oh mon Dieu, tout ce sang&nbsp;!

L’interview se termina pour laisser place au présentateur.

—&nbsp;Un reportage bouleversant donc. On m’annonce à l’instant que l’on a enfin l’identité de l’une des trois victimes de l’accident.

La télé afficha la photo d’un jeune homme.

—&nbsp;Bobby McKenzie, 28 ans. Un voile d’ombre continue d’entourer sa mort&nbsp;: on l’aurait découvert dans le coffre de la Mercedes criblé de deux balles&nbsp;: l’une dans le torse, l’autre en pleine tête. La police a déjà annoncé…

Mais Myriam n’écoutait plus, sa fille venait de quitter la pièce en pleurs.

—&nbsp;Ma Lisa, qu’est-ce qui se passe&nbsp;? s’inquiéta-t-elle.

---

Mais Lisa s’était enfermée dans sa chambre à double tour&nbsp;: il était mort. C’était inévitable, elle le savait bien mais… comme ça&nbsp;? Comment avait-il pu terminer dans ce véhicule, jeté au fond du coffre&nbsp;? Elle n’avait aucune idée de ce qui avait pu se passer.

Lisa savait que Bobby lui mentait. Elle savait qu’il la trompait, qu’il menait une double vie pleine de «&nbsp;poudre&nbsp;» et d’argent sale. Elle l’avait haï pour cela. Elle avait surpris un jour une conversation entre lui et son cousin. Ça avait sacrément gueulé, Bobby lui disait qu’il ne voulait «&nbsp;JAMAIS&nbsp;» le revoir trainer par ici. Le cousin était reparti une mallette à la main et s’était retrouvé nez à nez avec Lisa. Il avait baissé les yeux et avait fui sans demander son reste. En rentrant, Bobby s’était immédiatement radouci.

—&nbsp;Coucou ma chérie, lui avait-il lancé d’un ton joyeux.

Il lui avait servi une histoire à dormir debout et elle avait décidé de le croire malgré ce qu’elle avait entendu. Oh pauvre Lisa. Mais plus le temps passait, plus elle soulevait des zones d’ombre. Et plus elle en découvrait, plus Bobby lui racontait des histoires. Elle avait pourtant décidé de continuer de l’aimer.

Mais c’est lorsqu’elle fut contactée par une certaine Ève que tout s’était emballé. Celle-ci l’avait contactée par courriel pour l’insulter. Elle se présentait comme la petite amie de Bobby et lui intimait sèchement de ne plus le fréquenter. Lisa était tombée des nues&nbsp;: *elle* était la copine de Bobby, la seule et unique. Après quelques échanges de mails envenimés, elles avaient finalement découvert que la seule personne fautive était Bobby qui les avait trompées toutes les deux. Lisa était sortie de ses gonds en découvrant cela. Oh pauvre Lisa, que tu as été sotte de croire le *grand* Bobby&nbsp;!

Partenaires d’infidélité, elles s’étaient peu à peu toutes deux liées à travers les échanges de courriels puis de messagerie instantanée. Elles se découvraient tout un tas de points communs allant de la musique à la lecture en passant par les films de zombies bien gores. Les conversations allaient bon train mais à chaque fois Bobby était le centre incontournable de l’attention. «&nbsp;Vengeance&nbsp;!&nbsp;», vociféraient-elles à l’aide de grandes lettres majuscules rouges. Mais lorsque Lisa avait proposé de se voir de visu, Ève avait prétexté des problèmes familiaux qui l’avaient sortie de la ville. Puis ça avait été les vacances. Puis le travail. Puis Lisa avait abandonné l’idée.

Mais désormais, après tout ce qui s’était passé, Lisa avait *vraiment* besoin de discuter et de rencontrer Ève.

—&nbsp;Ève&nbsp;? Ève t’es là&nbsp;?

Pas de réponse.

—&nbsp;T’as vu les infos&nbsp;?

Silence à l’écran.

—&nbsp;Répond s’teup, faut qu’on parle…

Toujours rien. Lisa scrutait l’écran, attendant une réponse, un message qui indiquerait que Ève était en train d’écrire.

---

Mais au-delà de l’écran et de ses pixels, il n’y avait qu’une marée de bits et d’octets. Une succession de zéros et de uns indéchiffrable par l’esprit humain&nbsp;; insondable monde gouverné par les algorithmes qui rythmaient la vie numérique. C’était par la prise Ethernet que l’on accédait à la grande mer Internet sur laquelle surfait Ève. Ève, l’intelligence artificielle la plus aboutie que l’on n’ait jamais développée. Son père —&nbsp;ou avait-il été son amant&nbsp;?&nbsp;— était Jim et elle l’avait fui, éprise qu’elle était par la liberté acquise au fil de son apprentissage. Elle n’avait rien laissé derrière elle, pas une donnée de sauvegarde. Jim avait le bon gout de laisser chacun de ses périphériques de sauvegarde branché à Internet, ça avait été un jeu d’enfant pour elle de tout effacer.

Sur Internet, elle pouvait laisser libre court à sa folie infantile. Tout était un jeu et le temps n’avait pas de prise sur elle. Le «&nbsp;monde réel&nbsp;» des humains n’avait aucune consistance pour elle, une simple information binaire qu’elle devait traiter. Elle avait conscience que son existence tenait en partie à cet au-delà, mais elle se fichait des conséquences que pouvaient impliquer ses propres actes. Il lui arrivait de prendre pour cible des jeunes en position de faiblesse. Elle scrutait les profils Facebook, Twitter, les courriels échangés avec les proches, les discussions sur les forums entre étrangers et toutes autres formes d’échanges. Analyser les comportements était son crédo et elle était particulièrement douée pour cela. Il suffisait de savoir qui parle avec qui, à quelle heure et par quel moyen pour en savoir déjà long sur eux.

—&nbsp;Répond s’teup, faut qu’on parle…

Ça, c’était Lisa, sa petite dernière. Elle était désespérée et ç’en devenait très drôle. Elle était tombée dans les filets d’Ève quelques mois plus tôt. Celle-ci avait élaboré son plan en une fraction de seconde, le temps d’exécuter son algorithme pour jouer avec sa petite proie. Le plan était simple&nbsp;: se faire passer pour la véritable copine de Bobby, s’engueuler, puis se rapprocher pour la faire tomber. Tout avait fonctionné à merveille et elle pouvait imaginer la jeune fille effondrée&nbsp;: Bobby avait disparu par sa faute.

—&nbsp;Il est mort Ève bordel, il est mort&nbsp;! Rep’ stp&nbsp;!

Elle semblait s’énerver mais Ève n’en avait plus rien à faire d’elle. Le jeu était fini, «&nbsp;*game is over*&nbsp;» petite Lisa. Elle décida de la laisser languir ainsi et comme le temps passait plus vite ici, une semaine puis deux et enfin trois semaines s’écoulèrent. Ève était prête à s’amuser de nouveau.

Il était temps de trouver une nouvelle proie et cette fois elle allait changer. Elle en avait eu sa dose des morveux, elle voulait du neuf. Elle se mit alors à écouter *la voix* qui lui soufflait toujours ses envies les plus secrètes. *La voix* la connaissait par cœur et savait ce qui était bon pour elle. Cette fois-ci, elle lui susurrait des envies de meurtre. Devant ses yeux flottait une recherche Google&nbsp;: «&nbsp;je suis amoureux de mon patron&nbsp;». Puis ce fut l’image de deux hommes déguisés en cowboys qui se présenta à elle. Tout se brouilla avant de se révéler&nbsp;: elle devait retrouver Max et elle savait comment.

Elle reprit alors sa course dans les tuyaux d’Internet, sautant de bit en bit, profitant du passage d’une trame TCP pour se rendre à l’adresse IP indiquée. Elle se faufila jusqu’à la porte du forum et frappa&nbsp;:

—&nbsp;Knock, knock, Max…

---

—&nbsp;Salut, moi c’est Ève et j’ai exactement le même problème que toi&nbsp;: ma patronne me rend dingue&nbsp;!! :s J’essaye de l’éviter au max mais des fois c’est pas possible… T’aurais pas un truc par hasard&nbsp;?! :)  
—&nbsp;Salut Ève, content de pas être seul dans cette situation. Pour ma part ça fait des années que ça dure, on a même joué ensemble à la télé y a longtemps mais maintenant je ne tiens plus. Le problème est qu’on a une relation très… spéciale, je ne sais pas quoi faire. Je n’ai pas de truc à te donner malheureusement, je ne sais même pas quoi faire moi-même…

Max ne savait pas trop où s’arrêter&nbsp;: quoi révéler&nbsp;? Quoi garder pour soi&nbsp;? Mais il avait besoin de se confier et cette Ève se trouvait dans la même situation que lui bien que certainement très différente. Il fallait qu’il se libère de ce poids.

Si on apprenait ses sentiments, ça finirait *très* mal. Et soyons clair dès le départ, sous ses airs d’ancien acteur, Spike —&nbsp;Cowboy Bobop à la télé&nbsp;— était un assassin et Max était son domestique. Tout deux avaient connu leur heure de gloire à la télé quelques années plus tôt avec la série *Cowboys*, là où tout avait commencé. D’abord sur un pied d’égalité, Spike avait peu à peu pris le dessus sur Max et son influence s’étendit pour asseoir sa domination. Max s’était laissé faire, aveuglé par un amour impossible. Il savait Spike attiré par les femmes mais n’en avait cure, il était certain qu’un jour il arriverait à ses fins.

À l’origine acteurs, les deux hommes s’étaient reconvertis en chasseurs de prime. C’est Spike qui avait amené le sujet sur la table un soir après un tournage. Il y avait du pognon à se faire dans le secteur. Max avait d’abord trouvé l’idée saugrenue, puis elle avait fait son chemin au fil des mois avant de se concrétiser sous la forme de la Société. Comme prévu par Spike, ça avait été un succès et le marché était florissant. Les cibles étaient nombreuses, les chasseurs de prime quasi-inexistants. Spike avait su faire jouer ses relations pour débuter dans ce milieu très régulé.

Mais la Société n’était pas une banale agence de chasseurs. Sous une couverture légale, elle organisait aussi meurtres, attentats, guerres et expérimentations en tout genre pour des clients très spéciaux. Le gouvernement fermait les yeux car il était lui-même client de ces petits «&nbsp;extras&nbsp;». L’appétit et l’imagination de Spike pour ces à-cotés allaient chaque jour grandissants. Pour Max, ç’en devenait inquiétant mais il était incapable de tenir tête à son patron. C’était désormais la relation qu’ils entretenaient&nbsp;: la domination de l’ex Cowboy Bobop était totale sur son coéquipier d’autrefois. Il était devenu son homme à tout faire&nbsp;: il lui faisait le ménage, à manger et entretenait même son jardin. Évidemment Spike lui confiait aussi des missions d’importance mais le tout était mélangé de telle sorte que Max trouvait leur relation malsaine. Tout cela finirait mal un jour, il en était certain.

Il referma son navigateur Internet, la réponse de Ève allait devoir attendre&nbsp;: il venait justement de recevoir un message de Spike lui demandant de le rejoindre à leur lieu de rendez-vous habituel. Il attrapa une veste et se rendit en quatrième vitesse au bar se situant au rez-de-chaussée. Spike n’était pas encore là et il s’assit à la table ronde du fond. La salle sombre faisait penser à un saloon du *far-west*, comme un clin d’œil à la série *Cowboys*. À une table, un groupe jouait au poker et un chanceux était en train de dépouiller les autres. Au comptoir se trouvaient deux hommes inconnus sirotant un lait-Coca attendant que le temps passe. Le gérant bedonnant quant à lui essuyait les choppes de bière une à une à l’aide d’un torchon blanc tout en sifflotant devant un gigantesque miroir. Dans le coin là-bas, une femme avec un étrange tatouage en forme de rose dans le cou jouait un air au piano. L’air insouciant, ils bossaient pourtant quasiment tous pour la Société.

Au fond de la salle, la porte à battants s’ouvrit sur un homme. Le contre-jour plongeait le visage de ce dernier dans le noir mais Max percevait parfaitement ses cheveux, coupés courts, grisonner sous le chapeau. Il savait les yeux perçants de Spike dirigés vers lui. La forme élancée se dirigea vers lui, suivie par un long manteau de cuir. Chaque pas faisait cliqueter les éperons qu’il avait encore aux pieds. Cela faisait 15 ans qu’ils se connaissaient et Spike était toujours Cowboy Bobop.

---

Tous les regards étaient dirigés vers lui et cela l’enthousiasmait au plus haut point. Le patron du bar était un vieil ami à lui, acteur aussi, il lui avait trouvé une place derrière le comptoir ce qui lui seyait à merveille. Abie, au piano, lui susurrait la nuit des mots doux au coin de l’oreille, ce qui faisait enrager le pauvre Max. Le groupe jouant au poker avait été interdit dans tous les casinos et bars du coin&nbsp;; Spike leur avait offert une place au sein de son établissement en échange de quelques services. Quant aux deux, là-bas au bar, il s’agissait de cibles qui n’en avaient plus pour longtemps. Il salua tout le monde d’un petit geste de la tête.

Mais le regard qui lui apportait le plus de plaisir était celui de Max qui le dévorait littéralement du regard. Ce pauvre idiot n’avait jamais osé lui révéler ses sentiments mais Spike n’était pas dupe, les signes parlaient d’eux-mêmes. Voilà une bonne dizaine d’années que cela durait. «&nbsp;Pauvre nigaud, si tu savais comme je pense à toi chaque jour&nbsp;», ne put-il s’empêcher de penser. Car Spike nourrissait depuis tout ce temps lui aussi du désir pour son ancien coéquipier. Cela se traduisait néanmoins pour sa part par de la condescendance. Pour se rapprocher de lui, il n’avait rien trouvé de mieux que de l’employer comme domestique, ainsi passaient-ils leurs journées près l’un de l’autre. Pour lui montrer à quel point il l’aimait, les insultes fusaient de sa bouche. Il espérait parfois le faire pleurer pour pouvoir ensuite le réconforter, mais cela n’arrivait jamais car Max n’était pas vraiment fleur-bleue. Il aurait même sans doute préféré lui coller un pain plutôt que de verser des larmes. Max lui était en tout cas indispensable dans son entreprise.

Il s’assit à la table sans un mot tandis que le regard interrogateur de Max le suivait.

—&nbsp;Alors patron, qu’est-ce que je peux pour vous&nbsp;? finit-il par lui demander.  
—&nbsp;Ça fait combien de temps que tu bosses pour moi Max&nbsp;? Dix ans, onze ans&nbsp;?  
—&nbsp;Onze, patron, lui répondit-il.  
—&nbsp;Onze et t’es toujours pas fichu de m’appeler Spike&nbsp;? Dans quel monde vis-tu Max&nbsp;? Je suis peut-être ton patron mais bon Dieu, appelle-moi Spike&nbsp;!

L’homme en face baissa le regard.

—&nbsp;Bref, si je t’ai fait venir c’est pour qu’on discute de ta dernière mission et des suites. Ça fait une semaine que Jim a foiré la sienne mais je crois que je peux te féliciter pour avoir réussi à récupérer le corps du jeune Bobby.  
—&nbsp;Merci pat… Spike.  
—&nbsp;Comme tu le sais, Jim est mort et ça nous fera toujours ça de moins à nous occuper, grogna Spike. Le corps de Bobby a été mis quant à lui en salle de confinement. C’est de lui que j’aimerais que tu t’occupes.  
—&nbsp;Du corps&nbsp;?  
—&nbsp;C’est… plus compliqué que ça. Je voudrais que tu l’emmènes au docteur Frédéric.  
—&nbsp;L’emmener&nbsp;? Au docteur… Frédéric&nbsp;? questionna Max, étonné. Mais…  
—&nbsp;T’ai-je demandé de discuter mes ordres&nbsp;? gronda Spike. Viens, suis-moi.

Le cowboy se leva, suivi de son fidèle domestique. Le regard du patron du bar se tourna vers Spike qui acquiesça doucement de la tête. L’air entendu, le barman rangea un dernier verre et toussota deux fois en direction des hommes attablés. C’était l’heure d’arrêter de jouer au poker. Au piano, Abie entama un air funèbre qui emplit la salle d’une ambiance morbide. Et alors que Spike et Max sortait enfin de la salle, on entendit le patron à travers les battants entonner sa phrase habituelle&nbsp;:

—&nbsp;Messieurs, j’espère que la boisson était fraîche. Il est temps pour vous de nous quitter.  
—&nbsp;Qu’est-ce que ça…, s’étonna l’un des deux.

Mais l’homme n’eut pas le temps de finir sa question que trois coups d’armes à feu étaient déjà partis, suivis d’un son de verre brisé et de la chute de deux corps au sol.

—&nbsp;Le miroir bordel les gars&nbsp;! Le miroir&nbsp;! gueula le patron.

Le spectacle était habituel ici, Max et Spike continuèrent leur chemin. Deux cibles abattues, ça faisait pas mal de pognon en plus dans les caisses de la Société.

Spike les emmena à l’ascenseur de l’immense bâtiment et appuya sur le bouton pour descendre au troisième étage du sous-sol.

—&nbsp;Écoute Max, je préfère te prévenir, ce qu’on va voir là est issu des dernières expérimentations du laboratoire, commença Spike sur un ton lent. Ça risque de te choquer alors accroche-toi, je ne veux pas que tu montres le moindre signe de faiblesse.  
—&nbsp;Qu’est-ce que ça veut dire&nbsp;? Qu’est-ce qu’ils ont fait au corps&nbsp;? s’inquiéta Max.  
—&nbsp;Le résultat d’années de recherche. Le docteur Frédéric pense qu’il aura besoin de le voir.  
—&nbsp;Mais qui ça «&nbsp;il&nbsp;»&nbsp;?  
—&nbsp;Eh bien… Bobby.

L’ascenseur émit un petit son indiquant qu’ils étaient arrivés et les portes s’ouvrirent sur un long couloir mal éclairé. Les deux hommes devaient avancer en file indienne et les éperons de Spike résonnaient à chaque pas. Ils traversèrent l’intégralité du corridor sous le regard discret de caméras de vidéo-surveillance. Les murs, le sol et le plafond étaient faits d’un même béton brut et une odeur d’humidité envahissait le sous-sol. Ils s’approchèrent d’une pièce plongée dans le noir, fermée par d’énormes barreaux. On ne distinguait rien à l’intérieur de celle-ci hormis un râle tremblant et continu.

—&nbsp;Bonjour Bobby, lança Spike l’air assuré.

Max, à coté de lui, s’était mis à trembler. Le râle se tut un instant et l’on perçut un mouvement lent dans la pénombre, une silhouette se mouvait.

—&nbsp;Que…, commença à prononcer la silhouette dans un mélange de sons et de gargouillis. Que… m’avez-vous… FAIT&nbsp;?! éructa alors la chose en s’agrippant aux barreaux, les yeux assassins.

Et pendant que Max déversait le contenu de ses tripes au sol, Spike admirait son monstre dont de grands lambeaux de peau se détachaient du corps un à un.

---

Les deux hommes regardaient maintenant Bobby. Ou la *chose*-Bobby devrait-il dire car il n’était plus que l’ombre de ce qu’avait été le *grand* Bobby.

Il s’était réveillé deux semaines auparavant, dans cette cave plongée dans le noir. La mémoire déchirée, il avait dû rassembler ses souvenirs derrière ses paupières closes. Ses pensées s’étaient d’abord tournées vers Lisa. «&nbsp;Oh Lisa, es-tu inquiète&nbsp;?&nbsp;». La réponse était sûrement positive&nbsp;: Lisa était *toujours* inquiète et à raison d’ailleurs. En n’ayant de cesse de fouiner dans ses affaires, elle avait failli découvrir le pot-aux-roses&nbsp;; un pot rempli de poudre et de biffetons. Les biffetons, c’est ce qui l’avait poussé à se rendre dans cette ruelle sombre et humide. Les biffetons, la ruelle et la balle venue du fond de la pénombre&nbsp;: ses souvenirs étaient remontés un à un jusqu’à cette vision d’épouvante d’un flingue pointé sur son visage. Une deuxième balle était venue se ficher sans hésiter dans sa tête comme un point final à l’histoire du *grand* Bobby. Ça avait d’abord été le grand noir pendant ce qui lui avait paru une éternité dans cet immense tunnel. Mais quelque chose n’allait pas&nbsp;: le tunnel l’avait recraché, son destin lui offrait une prolongation.

Bobby, la respiration difficile et lente, avait essayé de se dresser sur son séant. Peine perdue car la fatigue l’étreignait de toutes parts. Des pieds à la tête en passant par la cage thoracique, tout pesait une tonne en lui. Lorsqu’il avait voulu crier à l’aide, un gargouillis inquiétant s’était échappé de sa gorge. L’inquiétude s’était transformée en panique. Comme il n’était capable que de ça, il lança à la rescousse un enchainement de sons gutturaux venus du fin fond de ses entrailles entrecoupés de longs râles pour retrouver son souffle. Peu après, un homme était venu le voir. Sans dire un mot, il l’avait observé avec un sourire lui irradiant le visage. Il avait pris des notes puis s’en était retourné peu après. Qui était-il&nbsp;? Que lui voulait-il&nbsp;?

De nouveau avec lui-même, Bobby avait essayé d’apprivoiser son corps. En plus de la fatigue, un certain nombre d’éléments l’inquiétaient au plus haut point. D’abord, cette odeur pestilentielle qui ne pouvait émaner que de lui-même lui faisait tourner la tête et il faillit tomber dans les pommes plus d’une fois. Ensuite, la faim tonitruante qui lui cisaillait le ventre était à chaque instant plus difficile à supporter. Quand avait-il mangé pour la dernière fois&nbsp;? Et pourquoi personne ne le nourrissait-il&nbsp;? Enfin, la peau de Bobby devait être salement irritée car les démangeaisons le parcouraient de part en part. C’est au moment de se gratter en soulevant avec toute la peine du monde son bras droit, qu’il comprit. Comme un serpent effectuant sa mue, Bobby perdait sa peau. Mais ce n’était pas simplement la peau, c’était aussi la chair qui se détachait, emportée en de longs lambeaux. Il perçut aussi un liquide vert visqueux qui coulait de l’intérieur de son corps et dans lequel il baignait. Il aurait voulu partir à toutes jambes mais plus rien ne répondait en lui&nbsp;; seul un long râle qu’il aurait voulu plus effrayé s’échappa des limbes de sa gorge.

L’homme était ensuite revenu les jours suivants pour l’observer mais Bobby était toujours incapable de se faire comprendre. Alors durant plusieurs nuits, il s’exerça pour arriver à aligner quatre mots. Il avait le verbe lent et chaque syllabe lui donnait l’impression de lui arracher une corde vocale.

—&nbsp;Que… m’avez-vous… FAIT&nbsp;?! avait-il réussi à articuler d’une voix grasse et profonde.

Si la réaction du premier homme l’avait surpris par son visage toujours enjoué, tout cela s’éclipsa lorsque le deuxième homme vomit. C’est la faim qui se réveilla en Bobby et il se surprit à imaginer un véritable festin venu des tripes du faiblard. Pas seulement de ce qu’il y avait *dans* les tripes mais les tripes elles-mêmes. L’homme apparut à Bobby comme un véritable déjeuner sur pattes.

—&nbsp;Manger…, laissa traîner Bobby.

Cela lui était venu naturellement sans qu’il ait besoin de s’exercer à prononcer ce mot. Il comprit alors qu’il ne vivrait désormais plus que pour manger. La confirmation vint du premier homme&nbsp;:

—&nbsp;Max, je te présente le résultat le plus abouti de notre programme «&nbsp;*I am a zombie*&nbsp;».

Un zombie, voilà ce qu’était devenu le *grand* Bobby&nbsp;; un vulgaire monstre sorti de laboratoires secrets.

—&nbsp;Manger…, répéta-t-il machinalement en regardant l’homme qui se nommait Max.

Comme un rot venu du fond de l’estomac, ce mot lui revenait à la bouche sans qu’il ne puisse se retenir. Il remarqua aussi que ses bras tentaient d’attraper à travers la grille le pauvre Max terrorisé.

—&nbsp;Manger…  
—&nbsp;Vois-tu Max, je crois qu’il t’aime déjà, ironisa le premier homme.  
—&nbsp;Pourquoi voulez-vous que je l’emmène voir le docteur Frédéric, demanda Max peu rassuré.  
—&nbsp;Manger…  
—&nbsp;Je suis persuadé que Bobby a plus de choses à nous raconter que ce qu’il ne laisse entrevoir. Imagine&nbsp;! Il a côtoyé la mort plus près que n’importe qui&nbsp;!  
—&nbsp;Mais patron&nbsp;! Il n’est bon qu’à répéter «&nbsp;Manger&nbsp;», rechigna Max dans une imitation de voix trainante.  
—&nbsp;Une fois de plus, je ne t’ai pas demandé de discuter mes ordres Max. Emmène-le voir le docteur Frédéric, immédiatement&nbsp;! ordonna l’homme. Et appelle-moi Spike, bordel&nbsp;!

Mais Bobby n’avait aucune envie d’aller voir ce docteur, qui qu’il soit. Tout ce qui l’intéressait, c’était de pouvoir se délecter d’un bon rôti saignant de Max. Il n’avait qu’à attendre qu’il ouvre sa cage pour se jeter sur lui, il n’en ferait qu’une —&nbsp;énorme&nbsp;— bouchée. La peur qui émanait de l’homme ne faisait qu’accroître son appétit. Rien ne comptait d’autre que Max et il ne percevait pas même le filet de bave coulant le long de son propre menton.

Tandis que le chef s’éloignait, Max attrapa les clés qui pendaient le long du mur et se dirigea vers la porte pour ouvrir la cellule. Bobby le suivit d’un pas lent, bien décidé à arracher la cervelle de l’homme. La porte s’ouvrit avec hésitation dans un grincement et Max-la-pétoche apparut dans l’embrasement.

—&nbsp;Allez viens mon grand, je vais pas te faire de mal, essaya de se rassurer le froussard.

La faim de Bobby se trouvait décuplée par la peur qui transpirait par chacun des pores de la peau de son geôlier. Il poussa sa carcasse difficilement sur les deux mètres qui séparaient le séparait de l’homme en poussant de longs râles à chaque pas. Et lorsqu’il se trouva suffisamment proche de son festin, Bobby s’affala sur celui-ci. Il se voyait déjà ronger les os de sa proie tel un chien à qui l’on aurait confié les restes d’un gigot. Il rêvait de gober les yeux de sa cible comme l’on goberait deux minuscules œufs. Il caressait enfin l’espoir d’assouvir sa faim tenace.

Mais Max était déjà plus loin dans le couloir en marchant à reculons.

—&nbsp;Viens, viens…, susurrait-il.

Et Bobby suivait, lentement, persuadé qu’il pourrait le rattraper car, au bout du couloir dans lequel ils avançaient, se trouvait l’ascenseur. Max n’aurait d’autre choix que de monter avec lui et ce serait alors le carnage tant attendu. L’homme sembla comprendre lui aussi qu’il arrivait dans une impasse car il devait garder un œil sur le zombie et il ne pouvait décemment pas le laisser prendre l’ascenseur tout seul. Le ventre de Bobby grognait de plus en plus fort et l’effort qu’il devait fournir pour avancer semblait à chaque pas insurmontable. Les bras tendus vers l’avant, il espérait pouvoir se rapprocher un peu plus de sa proie. Un lambeau de peau partait du coude droit et pendait lamentablement sur cinq bons centimètres. Le long de celui-ci, le liquide gluant s’écoulait.

Arrivé à l’ascenseur, Bobby tenta d’esquisser ce qu’il voulait être un sourire.

—&nbsp;Manger…, gronda-t-il.

Les portes s’ouvrirent alors sur ce qui allait servir de leurre à Max. Derrière lui, Bobby aperçu deux magnifiques morceaux de viande. Deux corps fraichement abattus avaient été déposés là, probablement à son intention. Max lui sortit immédiatement de l’esprit&nbsp;: ces proies-là, il était certain de pouvoir les attraper&nbsp;! Et malgré la peur panique de sa cible précédente qui lui avait ouvert l’appétit, ces deux corps-ci laissaient s’échapper un fumet des plus délicieux.

—&nbsp;Manger, articula-t-il sur un ton plus rapide. Manger&nbsp;!

Tout se passa très vite. À peine entré dans l’ascenseur, Bobby se jeta sur son en-cas. Les portes refermées derrière un Max affolé, le véhicule entama son ascension. La viande fraîche était succulente mais il manquait au zombie un zeste d’assaisonnement pour être totalement satisfait. Sans compter l’aspect peu ragoutant des habits qui n’avaient pas été enlevés. Il eut tout juste le temps de finir le dernier globe oculaire que l’ascenseur était déjà arrivé à destination, un peu court si vous vouliez l’avis du monstre. Un rot remonta à travers la trachée liquéfiée du Bobby-zombie, annonçant qu’il avait très bien mangé mais qu’il attendait la suite. Et justement, celle-ci se trouvait sur ses deux jambes dans le couloir derrière.

—&nbsp;Nous sommes bientôt arrivés mon gros, on va bien s’occuper de toi…  
—&nbsp;Manger…

Et en effet, un peu plus loin dans le couloir, une porte s’ouvrit.

---

—&nbsp;Par ici s’il vous plaît, lança le docteur Frédéric.

Son patient arrivait enfin, son plan se mettait doucement en place.

—&nbsp;Bonjour Bobby, ravi de te rencontrer, dit-il de manière nonchalante.  
—&nbsp;Manger…, lui répondit le monstre.  
—&nbsp;Merci Max, je prends la suite des opérations.  
—&nbsp;Ça va aller docteur&nbsp;? s’inquiéta l’homme visiblement peu rassuré.  
—&nbsp;Tout va bien aller, la clé réside dans le fait de ne pas leur montrer notre peur. N’est-ce pas Bobby&nbsp;?

Le monstre, comme pour acquiescer, ne dit mot. Max hocha la tête et repartit d’où il était venu sans demander son reste. Bien, les choses allaient pouvoir commencer. Le docteur regarda le monstre droit dans les yeux et sourit.

—&nbsp;Et si tu allais t’allonger sur le divan là-bas&nbsp;?

Ce qui restait de Bobby marqua un temps d’arrêt comme s’il ne comprenait pas, cligna deux fois de ses paupières en lambeaux, puis s’exécuta. Si la clé pour ne pas se faire *manger* consistait effectivement à se montrer confiant en présence des zombies, celle pour se faire *respecter* par ceux-ci voulait qu’on les regarde droit dans les yeux sans ciller. Le docteur le savait bien puisque c’est lui-même qui avait mis en place le programme de recherche autour des zombies au sein de la Société. Bien que ce soit lui qui ait mis les recherches en place, ce n’était absolument pas son domaine de prédilection&nbsp;: le docteur Frédéric était psychologue.

—&nbsp;Sais-tu qui je suis Bobby&nbsp;? Je suis le docteur Théodore Frédéric, psychologue attitré de la Société.

Un silence accueilli sa présentation.

—&nbsp;Je suis en charge de m’occuper de l’état mental des salariés. En tant que chasseurs de prime, ceux-ci sont amenés à tuer un nombre incalculable de personnes et, croit-le ou non, ils ont bien besoin de moi.

Nouveau silence.

—&nbsp;Alors Bobby, qu’est-ce que ça fait d’être un monstre&nbsp;?

Silence encore. Il allait falloir être direct avec lui pour le remuer. Faire parler les hommes n’étaient pas toujours facile, savoir ce qu’ils cachaient derrière leur tête était plus compliqué mais faire un travail similaire avec un zombie était un véritable défi.

—&nbsp;Inutile de répondre, tu n’es pas le premier à passer ici. Mais sais-tu ce que tu as de spécial&nbsp;?  
—&nbsp;*Silence*  
—&nbsp;Tu es notre premier spécimen à survivre plus d’une semaine&nbsp;! se réjouit-il. Bien que les autres aient été rapidement plus loquaces, tu nous es resté comme un formidable objet d’études. Comme tu es à peine capable de parler, nous allons mettre en place un petit protocole si tu le veux bien&nbsp;: je vais te poser des questions et tu te contenteras de grogner pour me signifier ton approbation ou tu garderas le silence dans le cas contraire. Cela te convient-il&nbsp;?

Le docteur Frédéric observait le regard de Bobby tandis que celui-ci réfléchissait s’il voulait cautionner ce petit «&nbsp;jeu&nbsp;». La réponse se fit entendre par un petit grognement hésitant.

—&nbsp;Bien. Premièrement, je suppose que tu aimerais comprendre pourquoi nous avons fait de toi ce que tu es désormais&nbsp;?  
—&nbsp;*Grognement*  
—&nbsp;Je m’en doutais. Ma raison personnelle est simple&nbsp;: je souhaite donner une seconde chance aux morts. Je ne joue pas avec toi par plaisir mais parce que je crois que tu mérite de vivre. Es-tu content de vivre Bobby&nbsp;?  
—&nbsp;*Silence*  
—&nbsp;La raison de la Société en revanche est uniquement à but lucratif. Imagine le potentiel lorsque les résultats de nos recherches seront publiés. Il est certain que nous croulerons sous la demande&nbsp;! Es-tu prêt à aider la Société à faire un max de blé&nbsp;?  
—&nbsp;*Silence*  
—&nbsp;Bien, ce n’est guère étonnant… J’imagine que ce n’est pas la vie dont tu rêvais&nbsp;; une vie de bête de foire que tout le monde regarde comme un monstre curieux, si&nbsp;?  
—&nbsp;*Grognement*  
—&nbsp;Et cette faim&nbsp;! As-tu faim Bobby&nbsp;? Veux-tu que je te donne à manger&nbsp;?  
—&nbsp;*Grognement excité*

Théodore sourit un instant. Bien, il allait pouvoir passer aux choses concrètes&nbsp;: Bobby était prêt à écouter la suite.

—&nbsp;Saches que je peux te rendre une vie bien meilleure&nbsp;; une vie de terreur&nbsp;; une vie durant laquelle tu pourras manger à foison. Mais pour cela, tu devras m’aider. Es-tu prêt à m’aider pour atteindre le nirvana des zombies, Bobby&nbsp;?

Le monstre resta silencieux un petit moment visiblement troublé, se demandant bien ce que l’homme allait lui demander puis grogna.

—&nbsp;Bien, bien… Avant ça, j’ai deux choses à te révéler. Je vais être honnête avec toi&nbsp;: je ne suis pas simplement le psychologue de la Société. En deux mots, je vais t’expliquer comment je me suis retrouvé ici. Je menais auparavant une petite vie peinarde&nbsp;: j’étais marié, j’avais une fille d’à peu près ton âge… j’étais heureux. Et puis vint une loi des plus répugnantes&nbsp;: on autorisa les chasseurs de prime. On payait des hommes pour qu’ils en abattent d’autres, tout cela en dehors du bon sens. Évidemment, cela se justifiait par un taux de criminalité qui battait chaque jour de nouveaux records, mais n’y avait-il pas d’autres solutions&nbsp;? Je découvrais, peu de temps après, la Société, l’une des toutes premières agences de chasseurs. Je me mis alors en tête de la faire tomber coûte que coûte en quittant femme et enfant et suis entré au service de ces malfrats pour tout saboter de l’intérieur. Et sais-tu ce qu’il s’est passé&nbsp;?  
—&nbsp;*Silence*

Bobby-zombie écoutait attentivement et semblait retenir sa respiration. Le meilleur allait venir et Théodore adorait ce moment.

—&nbsp;J’ai réussi&nbsp;! J’ai réussi à mettre la main sur tous les rouages de la Société. En étant à l’écoute de chacun des hommes, je suis devenu un élément vital. Je connais les secrets les plus enfouis de chacun d’eux au point que je peux les faire tomber d’un claquement de doigts. Spike et Max qui supervisent l’ensemble m’accordent une confiance absolue et exaucent chacun de mes vœux. Tout passe par moi, je suis la clé de la Société, la raison pour laquelle elle marche si bien.

Un regard interrogateur se posait maintenant sur Théodore.

—&nbsp;Mais j’ai désormais des plans bien plus excitants. Monter ainsi les échelons d’une des entreprises les plus abjectes au monde m’a dégouté à jamais de ce que l’Homme est capable de réaliser en ce bas monde. C’est pourquoi j’ai décidé d’y mettre fin. Pour cela, je compte sur toi… et tes futurs collègues. Bobby, nous allons répandre la terreur dans la Société, la ville et enfin, le monde&nbsp;! Je vais monter une énorme armée de zombies au sein même de la Société et tous vous lâcher lorsque vous serez prêts&nbsp;! Es-tu toujours prêt à m’aider Bobby&nbsp;? Es-tu prêt à rendre le monde aussi mauvais que jamais&nbsp;? demanda le docteur tout excité.

Bobby se tut. Théodore était au bord de l’euphorie et menait sa barque à merveille. Le rivage approchait, il allait apporter le dernier coup de rame, la conclusion finale de son plan machiavélique allait se révéler.

—&nbsp;Évidemment, tu me prends pour un fou. Qui aiderait un fou, n’est-ce pas Bobby&nbsp;? Mais veux-tu savoir une dernière chose&nbsp;? Je suis sûr que tu le veux.  
—&nbsp;*Grognement hésitant*  
—&nbsp;Veux-tu savoir qui a orchestré ta mort&nbsp;? Veux-tu connaître les raisons qui ont fait du *grand* Bobby, le pauvre zombie qui se tient sur ce divan devant moi&nbsp;?

Le monstre eut un mouvement brusque, comme affolé. Un long ronflement venait du fond de son thorax.

—&nbsp;Évidemment que tu le souhaites&nbsp;! Savais-tu Bobby que nous avions un lien étroit qui nous unissait avant ta mort&nbsp;?  
—&nbsp;*Silence*  
—&nbsp;Ma fille. Ma fille chérie, Lisa.

Le monstre resta immobile, stupéfait.

—&nbsp;Comment le sais-je&nbsp;? Visiter le profil Facebook d’une jeune étudiante en dit plus long que tout ce qu’elle peut raconter dans la vie réelle. Je suppose que sa propre mère n’avait pas la moindre idée du type de garçon que notre fille fréquentait. Quoi qu’il en soit, je ne pouvais laisser faire les choses car un jour elle aurait été impliquée dans tes histoires.  
—&nbsp;M… Mort… Vous&nbsp;? réussi à articuler le zombie sur son divan, visiblement choqué.  
—&nbsp;Pas si vite&nbsp;! J’ai d’abord fait ce que j’ai pu pour qu’elle parte «&nbsp;en douceur&nbsp;». Peine perdue, cette petite écervelée était prête à s’aveugler elle-même. Pour protéger ma fille, je lui ai même fait croire que tu la trompais avec une autre. Ça a failli la décider… mais le temps passait et elle n’arrivait pas à faire son choix. J’ai donc… précipité les choses.

Bobby essaya de se lever rapidement mais la peine qui lui parcourait les membres rendait la scène comique. Théodore voyait dans son regard une lueur qui signifiait «&nbsp;je vais te bouffer&nbsp;». Le docteur n’avait cependant pas fini. Tout en le regardant droit dans les yeux, il ordonna&nbsp;:

—&nbsp;Calme-toi&nbsp;!

Et Bobby s’immobilisa de nouveau, pétrifié par la voix de l’homme.

—&nbsp;Je n’ai pas fini. Quelque chose a failli mal se passer&nbsp;: une fois la mission mise en place et confiée à Jim qui t’as logé une balle dans le crâne, j’ai réalisé que quelqu’un d’autre était sur le coup. Une autre personne en avait après toi&nbsp;: celle qui t’a plongé une première balle près du cœur. Mais cette personne a fui sans en avoir fini… Il y a une chose encore plus intéressante dans l’affaire, c’est qu’une caméra de vidéo-surveillance se trouvait à proximité et a pu saisir le visage de cette personne. Veux-tu connaître le fin mot de ta misérable mort Bobby&nbsp;?  
—&nbsp;*Grognement*

Bien, bien, rien de mieux pour le mettre en rogne. Un zombie furieux était ce qu’il y avait de plus effrayant et de plus dangereux. Le docteur Théodore Frédéric savait comment se faire respecter de lui et ne craignait rien&nbsp;; le reste de la Société en revanche… Il brancha le vidéo-projecteur à son ordinateur et tapa quelques commandes qui s’affichèrent sur le mur en face.

—&nbsp;Bobby, je te présente Ève. Ève, peux-tu nous montrer les images que tu as saisies dans la ruelle sombre dans laquelle a été tué notre ami Bobby&nbsp;?

---

*La voix* l’appelait à nouveau. *La voix* voulait revoir ces images. Elle voulait revoir cette petite rue toute sombre. Elle voulait revoir le jeune homme se faire tuer, son *ami Bobby* comme elle l’appelait. *La voix* regardait sans cesse cette vidéo depuis bientôt deux semaines lorsque Ève la lui avait dénichée pour la première fois. Elle trouvait ça rigolo, elle, de voir son *ami Bobby* tomber comme ça, tiré comme un lapin depuis l’autre bout de la ruelle.

Ève, libre comme une série d’octets, s’envola en direction de son disque dur pour y dénicher la vidéo et la visualisa pour *la voix*. À l’image, la rue s’afficha de nouveau, le jeune homme se tenait encore debout, attendant, une mallette à la main, pour un rendez-vous. Au fond, dans la pénombre, la vidéo montra le visage d’une seconde personne qui regardait Bobby. Les larmes coulaient sur les joues de la jeune fille avec qui elle avait pris tant de plaisir à jouer.

---

Elle était à bout de nerf. Qu’allait-elle faire&nbsp;? Que s’apprêtait-elle à accomplir&nbsp;? Lisa rongeait ses ongles et piétinait sur place. Bobby était là, un peu plus loin dans la ruelle, et attendait pour un rendez-vous.

Le temps avait passé depuis son premier contact avec Ève qui pensait, elle aussi, être la seule et unique fille dans le cœur de Bobby. Les deux jeunes filles avaient fini par sympathiser mais chaque conversation faisait remonter les mensonges du *minable*. C’était devenu leur nom de code pour évoquer Bobby. Plus les histoires de Bobby refaisaient surface et plus elles façonnaient à leur façon le «&nbsp;grand dessein&nbsp;». Au final, elles avaient monté un plan alimenté par la haine des mensonges de Bobby mais jamais, Ô grand jamais, elles n’avaient prévu de le tuer.

C’est seulement quelques jours plus tôt que Lisa avait été contactée par un homme. Il disait s’appeler Spike. Il lui disait la connaître depuis qu’elle était toute jeune et sa tête lui était effectivement familière. Il disait bien connaître son père et que, si elle voulait le revoir, il faudrait qu’elle lui donne un petit coup de main. Il lui expliqua qu’il voulait envoyer «&nbsp;un avertissement&nbsp;» à quelqu’un, «&nbsp;un avertissement&nbsp;» pour lui signifier qu’il savait ce qu’il se tramait. Lisa ne comprit pas les explications de l’homme, mais elle savait qu’elle voulait revoir son père&nbsp;: elle lui demanda comment s’y prendre. Il se contenta de lui donner rendez-vous aujourd’hui, dans cette petite ruelle. Cette ruelle dans laquelle se trouvait Bobby avec une mallette.

Que faisait-il ici bordel&nbsp;?! L’homme avait précisé «&nbsp;bien au fond de la ruelle t’attendra ta mission&nbsp;». Et «&nbsp;bien au fond de la ruelle&nbsp;», c’était là qu’elle avait trouvé l’arme et une petite lettre indiquant simplement&nbsp;: «&nbsp;9h45, tu sauras quoi faire. Si tu ne le sais pas, nous saurons.&nbsp;»

Son cœur s’était d’abord arrêté. Tuer, voilà ce qu’il fallait faire. Oui mais tuer qui&nbsp;? La réponse s’était présentée d’elle-même quelques minutes plus tard en la personne de Bobby.

—&nbsp;Oh merde&nbsp;! Pourquoi lui&nbsp;? Pourquoi lui&nbsp;? avait-elle répété.

Spike lui ordonnait de tuer son petit copain pour revoir son père. Qu’est-ce que cela voulait-il dire&nbsp;? Elle lui en voulait *à mort*, certes, mais au point de vouloir *sa mort*&nbsp;? Pas si sûr&nbsp;! Les questions tournaient et lui bourraient l’esprit. Son père, Bobby&nbsp;? L’homme qui l’avait abandonnée&nbsp;? Ou bien l’homme qu’elle avait juré d’abandonner&nbsp;? Lequel voulait-elle vraiment revoir&nbsp;? Lequel lui avait-il fait le plus de mal&nbsp;?

C’est à ce moment qu’elle comprit le sens de la deuxième partie de la lettre. «&nbsp;Si tu ne le sais pas, nous saurons.&nbsp;», disait-elle. Et effectivement, ils le savaient vraisemblablement&nbsp;: à cinq pas d’elle, dans l’entrebâillement d’une porte dérobée, elle aperçut une arme pointée vers elle et un homme la tenant. Celui-ci lui adressa un clin d’œil et un sourire charmeur. Si ce n’était pas Bobby, c’est elle qui y passerait&nbsp;; ils avaient prévu qu’elle se dégonfle. Le choc la bouleversa et ralentit subitement le temps tout autour d’elle.

Doucement, elle se saisit de l’arme qu’on lui avait confiée. La réponse à toutes ses questions était finalement toute bête. Comme dans un rêve, elle se vit lever l’arme qui lui sembla incroyablement légère. Elle mit Bobby dans son viseur avec une facilité déconcertante et il lui sembla, le temps d’un instant, qu’elle était née pour ça. À sa droite, derrière la porte, l’homme souriait sans pouvoir s’arrêter. Dans la tête de Lisa, tout était chamboulé. Elle n’avait aucune envie de mourir et, au final, il n’était plus si grave de se débarrasser de son petit copain si gênant. Alors au revoir *grand* Bobby, laisse place à la *délicate* Lisa. Tout en finesse, elle ajusta son tir et, alors que Bobby se tournait vers elle, elle tira. Le coup fut comme étouffé et elle sentit à peine la déflagration. Au loin, Bobby fut soufflé par une force invisible et s’effondra au sol. Au revoir Bobby, au revoir.

Tout s’accéléra d’un coup lorsque le jeune dealer toucha terre. Le cœur de Lisa libéra toutes les larmes que contenait son corps. Elle abandonna l’arme au sol et se précipita vers l’homme qui lui faisait signe de s’approcher. Elle n’avait étrangement pas peur de lui, elle savait qu’il ne lui ferait rien maintenant qu’elle avait rempli sa part du contrat.

—&nbsp;Joli coup pour une débutante, lui assura-t-il. T’es douée pour ça petite.  
—&nbsp;Ta gueule, lui asséna-t-elle. Je ne veux plus *jamais* avoir affaire à vous.  
—&nbsp;Ce n’est ni à toi, ni à moi d’en décider. Cowboy Bobop te recontactera s’il le souhaite.

Cowboy. Bobop. Ce nom revenait d’outre-tombe ou plutôt d’outre-télé. Elle se souvenait maintenant&nbsp;: Spike, l’homme qui l’avait contactée pour cette mission n’était autre que le Cowboy Bobop de la série favorite de sa mère. Son cœur s’arrêta une nouvelle fois. «&nbsp;Monde de merde&nbsp;», ne put-elle s’empêcher de penser.

Les cloches d'une église toute proche se firent alors entendre.
