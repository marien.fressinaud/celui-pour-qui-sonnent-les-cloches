# Celui pour qui sonnent les cloches

Ce dépôt contient la nouvelle « Celui pour qui sonnent les cloches ». Celle-ci a été publiée à l'occasion du Ray's Day 2015 et il s'agit pour moi de ma première publication. Je serais ravi d'avoir vos retours.

Cette nouvelle est publiée sous licence CC-BY, c'est-à-dire que vous pouvez la lire, la partager et la modifier sans aucune restriction. Votre seule obligation sera de me citer comme l'auteur de celle-ci en précisant l'adresse de mon site (http://marienfressinaud.fr).

Je remercie l'équipe Framasoft pour leur relecture attentive et la publication à leur côté.

---

Le texte est écrit en Markdown et un PDF est généré à partir de la ligne de commande suivante :

```sh
$ pandoc celui-pour-qui-sonnent-les-cloches.md -f markdown_mmd -t latex -o celui-pour-qui-sonnent-les-cloches.pdf
```
